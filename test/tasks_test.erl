%%%-------------------------------------------------------------------
%%% @author kleen
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. Авг. 2016 17:27
%%%-------------------------------------------------------------------
-module(tasks_test).
-author("kleen").

-include_lib("eunit/include/eunit.hrl").

first_task_test() ->
  ?assert(tasks:first_task(10, 3, 5) =:= 23),
  ?assert(tasks:first_task(1000, 3, 5) =:= 233168).

second_task_test() ->
  ?assert(tasks:second_task(1) =:= 0),
  ?assert(tasks:second_task(2) =:= 0),
  ?assert(tasks:second_task(3) =:= 2),
  ?assert(tasks:second_task(4) =:= 2),
  ?assert(tasks:second_task(10) =:= 10),
  ?assert(tasks:second_task(11) =:= 10),
  ?assert(tasks:second_task(150) =:= 188),
  ?assert(tasks:second_task(160) =:= 188),
  ?assert(tasks:second_task(1500) =:= 798).

third_task_test() ->
  ?assert(tasks:third_task(1) =:= 1),
  ?assert(tasks:third_task(2) =:= 2),
  ?assert(tasks:third_task(3) =:= 3),
  ?assert(tasks:third_task(4) =:= 2),
  ?assert(tasks:third_task(5) =:= 5),
  ?assert(tasks:third_task(8) =:= 2),
  ?assert(tasks:third_task(13195) =:= 29).

fourth_task_test() ->
  [R, F1, F2] = tasks:fourth_task(),
  [[A,B,C|Tail]|_] = io_lib:format("~p", [R]),
  ?assert([A,B,C] =:= lists:reverse(Tail)),
  ?assert(F1*F2 =:= R).

fifth_task_test() ->
  ?assert(tasks:fifth_task(10) =:= 2520),
  Result = tasks:fifth_task(20),
  Pass = fun Fun(0) -> 0;
             Fun(X) -> ?assert((Result rem X) =:= 0),
                      Fun(X-1)
         end,
  Pass(20).