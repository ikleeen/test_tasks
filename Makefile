
all: compile

compile: clean 
	@( erlc -DTEST -o ./ebin ./src/*.erl )
	@( erlc -DTEST -o ./ebin ./test/*.erl )
	

run:	compile
	@( erl -pa ebin )

clean:
	@( rm -f ebin/*.beam )
	@( rm -f erl_crash.dump )

test: 
	@( erl -noshell -eval 'eunit:test("ebin",[verbose])' -s init stop )

.PHONY:  test clean compile run

