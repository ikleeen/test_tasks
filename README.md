# Tasks 1-5 solutions

### Build
-------------
Compile module:
```sh
    $ make all
```
Run tests:
```sh
    $ make test
```
### Run
-------------
To run erlang 
```sh
    $ make run
```
Run module functions
```sh
    $ tasks:first_task(1000,3,5).
    $ tasks:second_task(4000000).
    $ tasks:third_task(600851475143).
    $ tasks:third_task(600851475143).
    $ tasks:fourth_task().
    $ tasks:fifth_task(20).
```